/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Data source for the number of solar panels in the habitat.
*/

import Foundation

struct CoughDataSource {
    /// Possible values for solar panels in the habitat
    let values = [1, 2, 3, 4]
    
    func title(for index: Int) -> String? {
        guard index < values.count else { return nil }
        return String(values[index])
    }
    
    func value(for index: Int) -> Double? {
        guard index < values.count else { return nil }
        return Double(values[index])
    }
}
