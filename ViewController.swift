/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Main view controller for the Covid-19-iOS app. Uses a `UIPickerView` to gather user inputs.
   The model's output is the predicted price.
*/

import UIKit
import CoreML
import CoreBluetooth
import AudioToolbox


private func AudioQueueInputCallback(
    _ inUserData: UnsafeMutableRawPointer?,
    inAQ: AudioQueueRef,
    inBuffer: AudioQueueBufferRef,
    inStartTime: UnsafePointer<AudioTimeStamp>,
    inNumberPacketDescriptions: UInt32,
    inPacketDescs: UnsafePointer<AudioStreamPacketDescription>?)
{
    // Do nothing, because not recoding.
}


class ViewController: UIViewController, CBCentralManagerDelegate,CBPeripheralManagerDelegate {
    // MARK: - Properties
    
  //  @IBOutlet weak var loudLabel: UILabel!
    
  //  @IBOutlet weak var peakTextField: UITextField!
  //  @IBOutlet weak var averageTextField: UITextField!

    var queue: AudioQueueRef!
    var timer: Timer!
    var noisebox:[Float32] = []
    var noises:Double = 0
    var noisebox2 = [Double](repeating: -60, count: 14)
    var noisenumber:Double = 0

    /*
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopUpdatingVolume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/

    // MARK: - Internal methods

    func startUpdatingVolume() {
        // Set data format
        var dataFormat = AudioStreamBasicDescription(
            mSampleRate: 44100.0,
            mFormatID: kAudioFormatLinearPCM,
            mFormatFlags: AudioFormatFlags(kLinearPCMFormatFlagIsBigEndian | kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked),
            mBytesPerPacket: 2,
            mFramesPerPacket: 1,
            mBytesPerFrame: 2,
            mChannelsPerFrame: 1,
            mBitsPerChannel: 16,
            mReserved: 0)

        // Observe input level
        var audioQueue: AudioQueueRef? = nil
        var error = noErr
        error = AudioQueueNewInput(
            &dataFormat,
            AudioQueueInputCallback,
            UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()),
            .none,
            .none,
            0,
            &audioQueue)
        if error == noErr {
            self.queue = audioQueue
        }
        AudioQueueStart(self.queue, nil)

        // Enable level meter
        var enabledLevelMeter: UInt32 = 1
        AudioQueueSetProperty(self.queue, kAudioQueueProperty_EnableLevelMetering, &enabledLevelMeter, UInt32(MemoryLayout<UInt32>.size))

        self.timer = Timer.scheduledTimer(timeInterval: 0.5,
                                                            target: self,
                                                            selector: #selector(ViewController.detectVolume(_:)),
                                                            userInfo: nil,
                                                            repeats: true)
        self.timer?.fire()
    }

    func stopUpdatingVolume()
    {
        // Finish observation
        self.timer.invalidate()
        self.timer = nil
        AudioQueueFlush(self.queue)
        AudioQueueStop(self.queue, false)
        AudioQueueDispose(self.queue, true)
    }

    @objc func detectVolume(_ timer: Timer)
    {
        // Get level
        var levelMeter = AudioQueueLevelMeterState()
        var propertySize = UInt32(MemoryLayout<AudioQueueLevelMeterState>.size)

        AudioQueueGetProperty(
            self.queue,
            kAudioQueueProperty_CurrentLevelMeterDB,
            &levelMeter,
            &propertySize)

        // Show the audio channel's peak and average RMS power.
    //    self.peakTextField.text = "".appendingFormat("%.2f", levelMeter.mPeakPower)
    //    self.averageTextField.text = "".appendingFormat("%.2f", levelMeter.mAveragePower)

        noisebox.append(levelMeter.mAveragePower)
        
        var total:Float32 = 0
        
        for value in noisebox {
            total = total + value
        }
        let count = Float32(noisebox.count)
        noises = Double(total/count)
        
        
       // print(noise)
        
        // Show "LOUD!!" if mPeakPower is larger than -1.0
     //   self.loudLabel.isHidden = (levelMeter.mPeakPower >= -1.0) ? false : true
    }
    
    
    
    var centralManager: CBCentralManager!
    @IBOutlet weak var deviceButton: UIButton!
    @IBOutlet weak var device: UILabel!
    @IBOutlet weak var device_number: UILabel!
    @IBOutlet weak var RSSI_number: UILabel!
    @IBOutlet weak var noise_number: UILabel!
    @IBOutlet weak var date: UILabel!
    
    
    var peripheralManager: CBPeripheralManager!
    
    var party: [String] = []
    var count:Int = 0   //デバイス数
    var countnumber:Int = 0
    var countbox = [Int](repeating: 0, count: 14)
    var result:Int = 0     //全検査結果数
    var i:Int = 0 //くり返し
    var t:Int = 0
    var k:Int = 0
    var h:Int = 0
    var rssibox:[Double] = []
    var rssibox2 = [Double](repeating: -65, count: 14)
    var rssi:Double = 0
    var rssinumber:Double = 0
    
    
    
    @IBAction func tap(_ sender: Any) {
        
        for h in 0...100 {
            
        k=0
            
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        self.startUpdatingVolume()
        
            for k in 0...13 {
        
                DispatchQueue.main.asyncAfter(deadline: .now() +  86400 + 86400 * Double(k) + 1209600 * Double(h)) { [self] in
            //stopBluetoothScan()
           // self.stopUpdatingVolume()
                    
                    countbox[k] = count
                    countnumber = countbox[0]+countbox[1]+countbox[2]+countbox[3]+countbox[4]+countbox[5]+countbox[6]+countbox[7]+countbox[8]+countbox[9]+countbox[10]+countbox[11]+countbox[12]+countbox[13]
                    device_number.text = String(countnumber)
                    rssibox2[k] = rssi
                    rssinumber =
                        (rssibox2[0]+rssibox2[1]+rssibox2[2]+rssibox2[3]+rssibox2[4]+rssibox2[5]+rssibox2[6]+rssibox2[7]+rssibox2[8]+rssibox2[9]+rssibox2[10]+rssibox2[11]+rssibox2[12]+rssibox2[13])/14
                    RSSI_number.text = String(rssinumber)
                    noisebox2[k] = noises
                    noisenumber =
                        (noisebox2[0]+noisebox2[1]+noisebox2[2]+noisebox2[3]+noisebox2[4]+noisebox2[5]+noisebox2[6]+noisebox2[7]+noisebox2[8]+noisebox2[9]+noisebox2[10]+noisebox2[11]+noisebox2[12]+noisebox2[13])/14
                    noise_number.text = String(noisenumber)
                    updatePredictedPrice()
                    date.text = "\(h*14+(k+1))日間アプリを使用しています"
            }
                count = 0
        }
            
    }
 
 
    }
    
    
    @IBAction func stop(_ sender: Any) {
        stopBluetoothScan()
        self.stopUpdatingVolume()
        device_number.text = String(count)
      //  device_number.text = String(0)
        RSSI_number.text = String(rssi)
      //  RSSI_number.text = String("non")
        noise_number.text = String(noises)
        updatePredictedPrice()
    }
    
    
    
   /* @IBAction func deviceTap(_ sender: Any) {
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        self.startUpdatingVolume()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [self] in
            stopBluetoothScan()
            self.stopUpdatingVolume()
            device.text = String(count)
        }
    }*/
    
 
    override func viewWillDisappear(_ animated: Bool) {
        centralManager.stopScan()
        print("Scanning stopped")
        
        super.viewWillDisappear(animated)
    }
    
    // CentralManager status
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("CBManager is powered on")
            startScan()
        case .poweredOff:
            print("CBManager is not powered on")
            return
        case .resetting:
            print("CBManager is resetting")
        case .unauthorized:
            print("Unexpected authorization")
            return
        case .unknown:
            print("CBManager state is unknown")
            return
        case .unsupported:
            print("Bluetooth is not supported on this device")
            return
        @unknown default:
            print("A previously unknown central manager state occurred")
            return
        }
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        let enumName = "CBPeripheralManagerState"
        var valueName = ""
        switch self.peripheralManager.state {
        case .poweredOff:
            valueName = enumName + "PoweredOff"
        case .poweredOn:
            valueName = enumName + "PoweredOn"
            startAdv()
        case .resetting:
            valueName = enumName + "Resetting"
        case .unauthorized:
            valueName = enumName + "Unauthorized"
        case .unknown:
            valueName = enumName + "Unknown"
        case .unsupported:
            valueName = enumName + "Unsupported"
        @unknown default:
            print("A previously unknown central manager state occurred")
        }
        print(valueName)
    }
    
    var advertisementData: Dictionary = [CBAdvertisementDataLocalNameKey: UIDevice.current.name]
    

    // begin to scan
    func startScan(){
        print("begin to scan ...")
        centralManager.scanForPeripherals(withServices: nil)
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        print("アドバタイズ成功！")
    }
    
    func startAdv(){
        print("begin to advertizing ...")
        peripheralManager.startAdvertising(self.advertisementData)
    }
    
    // stop scan
    func stopBluetoothScan() {
        self.centralManager.stopScan()
    }
    
    
    // Peripheral探索結果を受信
    func centralManager(_ central: CBCentralManager,
                        didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any],
                        rssi RSSI: NSNumber) {
    
        
        var flag:Int = 0
        if peripheral.name != nil {
        print("pheripheral.name: \(String(describing: peripheral.name))")
        print("advertisementData:\(advertisementData)")
        print("RSSI: \(RSSI)")
            rssibox.append(RSSI.doubleValue)
            rssi = rssibox[0]
        print("peripheral.identifier.uuidString: \(peripheral.identifier.uuidString)\n")
        party.append("\(String(describing: peripheral.name))")
            if result > 0 {
                for i in 1...result{
                        if party[result] == party[result-i]{
                            flag = 1
                            //  print("検査が重複しました")
                        }else{
                        }
                    }
                if flag != 1{
                print("デバイス数が加算されました")
                count += 1
                }
            } else {
                print("デバイス数が加算されました")
                count += 1
            }
            print("デバイス数:\(count)")
            result+=1
        } else {
            print("周辺機器の名前を取得できませんでした")
        }
        
        /*
        if rssibox.count >= 2{
            for t in 1...rssibox.count-1{
                if rssibox[t] >= rssi{
                    rssi = rssibox[t]
                }else{
                }
            }
        }else{
        }
 */
        
        var totalrssi:Float32 = 0
        
        for value in rssibox {
            totalrssi = totalrssi + Float(value)
        }
        let count = Float32(rssibox.count)
        rssi = Double(totalrssi/count)
        
        print("平均RSSI:\(rssi)")
        print(noises)
    }
    
    
    
 
    //    static let instance = ViewController()
    
    
      let model = data18()
      
      /// Data source for the picker.
      let pickerDataSource = PickerDataSource()
      
      /// Formatter for the output.
      let priceFormatter: NumberFormatter = {
          let formatter = NumberFormatter()
          
          formatter.numberStyle = .decimal
          formatter.maximumFractionDigits = 3
          formatter.usesGroupingSeparator = true
          /*
          formatter.numberStyle = .currency
          formatter.maximumFractionDigits = 0
          formatter.locale = Locale(identifier: "en_US")
   */
          return formatter
      }()
      
      
      // MARK: - Outlets

      /// Label that will be updated with the predicted price.
      @IBOutlet weak var priceLabel: UILabel!
      @IBOutlet weak var warning: UILabel!
    

      /**
           The UI that users will use to select the number of solar panels,
           number of greenhouses, and acreage of the habitat.
      */
      @IBOutlet weak var pickerView: UIPickerView! {
          didSet {
              pickerView.delegate = self
              pickerView.dataSource = pickerDataSource
            let features: [Feature] = [.cough/*, .body_temperature,.throat, .nose, .lazy*/]
              for feature in features {
                  pickerView.selectRow(2, inComponent: feature.rawValue, animated: false)
              }
          }
      }
      
      // MARK: - View Life Cycle
      
      /// Updated the predicted price, when created.
      override func viewDidLoad() {
          super.viewDidLoad()
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
          updatePredictedPrice()
      }
      /**
           The main logic for the app, performing the integration with Core ML.
           First gather the values for input to the model. Then have the model generate
           a prediction with those inputs. Finally, present the predicted value to
           the user.
      */
      func updatePredictedPrice() {
          func selectedRow(for feature: Feature) -> Int {
              return pickerView.selectedRow(inComponent: feature.rawValue)
          }

          let cough = pickerDataSource.value(for: selectedRow(for: .cough), feature: .cough)
          let body_temperature = pickerDataSource.value(for: selectedRow(for: .body_temperature), feature: .body_temperature)
          let throat = pickerDataSource.value(for: selectedRow(for: .throat), feature: .throat)
          let nose = pickerDataSource.value(for: selectedRow(for: .nose), feature: .nose)
          let lazy = pickerDataSource.value(for: selectedRow(for: .lazy), feature: .lazy)
            let devise = Double(count)
         // let devise = Double(0)
         // let devise = Double(100)
            let noise = noises
         // let noise = Double(-29.26546)
            var RSSI = rssi
         // var RSSI = Double(-35.35678)
        
        if devise == 0{
            RSSI = -95
        }
        
          guard let data6Output = try? model.prediction(cough: cough, body_temperature: body_temperature,throat: throat, nose: nose, lazy_: lazy, device: devise,noise: noise, RSSI:RSSI) else {
              fatalError("Unexpected runtime error.")
          }
          
          let result = data6Output.classProbability
          print(result)
          let proba = result[1]!*100
        priceLabel.text = priceFormatter.string(for: proba)! + "%"
        if proba >= 90 {
            warning.text = "COVID-19に感染している可能性がかなり高いです\nPCR検査を受診することを強く推奨します"
        } else if proba >= 70 {
            warning.text = "COVID-19に感染している可能性が高いです\nPCR検査を受診することを強く推奨します"
        } else if proba >= 50 {
            warning.text = "C0VID-19に感染している可能性があります\nPCR検査を受診することを推奨します"
        } else if proba >= 30 {
            warning.text = "COVID-19に感染している可能性が少しあります\nPCR検査を受診することを少しだけ推奨します"
        } else {
            warning.text = ""
        }
      }
  }
