作成者：金井 大河

これは筆者が卒業研究で行った「ロジスティック回帰分析を用いた新型コロナウイルス感染確率算出アプリケーションの開発」をテーマに研究を行った成果をまとめたものである．


・環境の構築
①	Xcodeのインストール（Apple StoreやApple Developerサイト）
XcodeはMacのみ対応しているので、Windowsではインストールできない。
インストールは一般的にはAppleStore(https://itunes.apple.com/jp/app/xcode/id497799835?mt=12)で行うが、Apple Developerに登録をされている方であれば、Apple Developerサイトからも、インストールが可能である。
Appleのdeveloperサイトからdmgをダウンロード&インストール
・AppleのDeveloperサイトの中で、Xcodeのページ(https://idmsa.apple.com/IDMSWebAuth/signin?appIdKey=891bd3417a7776362562d2197f89480a8547b108fd934911bcbea0110d07f757&path=%2Fdownload%2F&rv=1)にアクセス。
・ページ下部の「See more downloads」というリンクをクリック。
・Apple IDを入力してログイン。
・欲しいアプリケーションの左側の「＋」の部分をクリック。
・dmgのダウンロードリンクが現れるので、クリックしてダウンロード。
・dmgを実行してインストール
②	Spyderのインストール
csvファイルをロジスティック回帰分析するためにpython上でコードを作成した。
Spyderでなくとも、pythonが動作するような環境が構築できていれば実行可能である。
Spyderの導入方法
・Anacondaのサイト(https://www.anaconda.com/products/individual#download-section)からインストーラをダウンロードする。
・OSとビット数に合わせて、適切なものを選択する。
・ダウンロードしたインストーラ(Anaconda Navigator)を実行する。
・Anaconda NavigatorのホームからSpyderをLaunchする。
以上で実行環境の構築は完了である。

・実行方法
研究成果フォルダのdata9.csvをXcodeで扱える形式であるdata18.mlmodelに変換するためにlogistic.pyファイルのコードをpython実行環境下で実行する。
得られたmlmodelをCovid-19-iOSのResourseフォルダに挿入する(研究成果フォルダのCovid-19-iOSではmlmodelが既に入っているので挿入する必要はなく、pythonのコードも実行する必要はない)。
USBなどでMacとiPhoneを接続し、Covid-19-iOS.xcodeprojファイルを起動後、Xcode上の画面のSet the active schemeにて自分のiPhoneを選択し、その左側にある▶︎ボタンを押し、コードを実行する。

ロジスティック回帰モデルの評価の出力方法は、accuracy.pyのコードをpython実行環境下で実行する。




以下にアプリケーションの内容（主にCoreMLModel：コアMLモデル）について解説する。


＃コアMLモデルをアプリに統合する

ロジスティック回帰モデルをアプリに追加し、
入力データをモデルに渡し、モデルの予測を処理する。

##概要

このアプリは、トレーニング済みモデル `data18.mlmodel`を使用している。
Covid-19の感染確率を予測するためのモデルである。

## Xcodeプロジェクトにモデルを追加する

モデルをプロジェクトにドラッグして、Xcodeプロジェクトにモデルを追加する。

このアプリでの入力は、咳の有無、体温、喉の有無、鼻水の有無、だるさの有無をそれぞれ4段階で
選択入力したもの、「周囲の三密度をチェック」のボタンを押すことによって取得した、すれ違ったデバイス数、RSSI、騒音レベルである。

出力は、Covid-19の感染確率である。

##コードでモデルを作成する

Xcodeは、モデルの入力と出力に関する情報も使用して、
モデルへのカスタムプログラムインターフェイスを自動的に生成し、
カスタムプログラムインターフェイスを使用して、コード内のモデルを操作する。

``` swift
let model = data18()
```

##モデルに渡す入力値を取得する

このアプリは、 `UIPickerView`を使用して、ユーザーからモデルの入力値を取得するのと同時に、BLEやマイクを用いて自動で入力値を取得する。


``` swift
func selectedRow(for feature: Feature) -> Int {
    return pickerView.selectedRow(inComponent: feature.rawValue)
}

let cough = pickerDataSource.value(for: selectedRow(for: .cough), feature: .cough)
let body_temperature = pickerDataSource.value(for: selectedRow(for: .body_temperature), feature: .body_temperature)
let throat = pickerDataSource.value(for: selectedRow(for: .throat), feature: .throat)
let nose = pickerDataSource.value(for: selectedRow(for: .nose), feature: .nose)
let lazy = pickerDataSource.value(for: selectedRow(for: .lazy), feature: .lazy)

let device = Double(count) //countは取得したデバイス数
let noise = noises　//noisesは取得した騒音レベル
var RSSI = rssi　//rssiは取得したRSSI

```


##モデルを使用して予測を行う

予測に使用される要素は以下の8つである。
 `prediction（cough：body_temperature：throat：nose：lazy：device：noise：RSSI：）`
それぞれが、cough：咳、body_temperature：体温、throat：喉、nose：鼻水、lazy：だるさ、device：デバイス数、noise：騒音レベル、RSSI：電波強度
に対応している。


``` swift
guard let data6Output = try? model.prediction(cough: cough, body_temperature: body_temperature,throat: throat, nose: nose, lazy_: lazy, device: devise,noise: noise, RSSI:RSSI) else {
    fatalError("Unexpected runtime error.")
}
```


`data6Output`の classProbability プロパティにアクセスして、予測した感染確率を取得する。


``` swift
let result = data6Output.classProbability
let proba = result[1]!*100
```

##コアMLアプリをビルドして実行する

Xcodeはデバイス上で実行するように最適化されたリソースにコアMLモデルをコンパイルする。
このモデルは、アプリバンドルに含まれており、アプリがデバイスで実行されているときに予測を行うために使用されるものである。
